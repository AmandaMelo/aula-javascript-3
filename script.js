let enviar = document.getElementById("submit");
let divMsgs = document.getElementById("msgs");
let num = 1;


enviar.addEventListener('click', printMessage);

function printMessage(e) {
    let nome = document.forms["formulario"]["name"].value;
    let conteudo = document.forms["formulario"]["message"].value;

    // criando span responsável pelo post
    let spanMsg = document.createElement('span');
    spanMsg.classList.add('msg');
    spanMsg.id= `${num}`;
    
    let linha = document.createElement('div');
    linha.classList.add('linha');
    linha.innerHTML = `<div class="buttons"><button type="button" name="delOne" onclick=deleteThisMessage(${spanMsg.id}) /></button>
    <button type="button" name="editOne" onclick=editThisMessage(${spanMsg.id}) /></button></div>`
 

    // criando as divs
    let pessoa = document.createElement('div');
    let fala = document.createElement('div');
    

    // classificando as respectivas divs
    pessoa.classList.add('nome');
    pessoa.id=`p${num}`;
    fala.classList.add('conteudo');
    fala.id=`c${num}`;

    // conteudo do post
    pessoa.innerText = nome;
    fala.innerText = conteudo;

    // adicionando ao html
    divMsgs.appendChild(spanMsg);
    spanMsg.appendChild(linha);
    linha.appendChild(pessoa);
    spanMsg.appendChild(fala);
    num += 1;

    e.preventDefault();
};

let delet = document.getElementById("del");

delet.addEventListener('click', deleteAll);
function deleteAll(e) {
    let mensagens = document.querySelectorAll("span");
    mensagens.forEach(msg => msg.remove());
};

function deleteThisMessage(e) {
    let spanMsg = document.getElementById(e);
    spanMsg.remove();
};

function editThisMessage(e) {
    let spanMsg = document.getElementById(e);
    // criar espaço para edição da mensagem
    let edicao = document.createElement('div');
    edicao.classList.add('edicao');
    edicao.innerHTML=`<label for="name" class="boxE">Editar nome: <input class="text-edit area" type="text" name="name" id="novoNome"/></label><label for="message" class="boxE">Editar Mensagem: <textarea class="text-edit" type="text" name="message" id="novaMsg" rows="5"></textarea></label> <div class="buttons2"> <input type="button" name="edit" id="edit" value="Editar" onclick="sendEdit(${spanMsg.id})"/></div>`
    spanMsg.appendChild(edicao);
}

function sendEdit(e) {
    let pessoa = document.getElementById(`p${e}`);
    let conteudo = document.getElementById(`c${e}`);

    // modificando conteúdo da mensagem
    pessoa.innerText = document.getElementById('novoNome').value;
    conteudo.innerText = document.getElementById('novaMsg').value;

    cancelar(e);
}

function cancelar(e) {
    let spanMsg = document.getElementById(e);
    let edicao = spanMsg.lastChild;
    console.log(edicao);
    edicao.remove();
}
